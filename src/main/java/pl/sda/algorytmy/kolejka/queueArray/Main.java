package pl.sda.algorytmy.kolejka.queueArray;

public class Main {
    public static void main(String[] args) {

        Queue queue = new Queue(5);

        System.out.println(queue.isEmpyt());
        queue.push(4);
        System.out.println(queue.isEmpyt());
        queue.pop();
        queue.push(25);

        queue.peek();

        queue.push(123456);
        queue.push(456);
        queue.pop();
        queue.peek();
        queue.pop();
        queue.peek();
        queue.pop();
        queue.peek();
        queue.pop();
        queue.peek();
        queue.pop();
        queue.peek();

}
}
