package pl.sda.algorytmy.kolejka.queueArray;

public class Queue {

    Object[] queue;
    int maxSize;
    int index;

    public Queue(int maxSize) {
        this.maxSize = maxSize;
        this.index = -1;
        this.queue = new Object[maxSize];
    }

    public void push(Object obj){
        if(index <= maxSize){
            index++;
            queue[index] = obj;
        }else{
            System.out.println("Kolejka jest pełna, nie można dodoać elelemtnów");
        }
    }

    public void peek(){
        System.out.println(queue[0]);
    }

    public void pop(){
        if (index > -1){
        for (int i = 0; i < index; i++) {
            queue[i]= queue[i+1];
        } queue[index]=null;
        index--;
        }else{
        System.out.println("Kolejka jest już pusta, dodaj elementy do niej");
        }
    }

    public boolean isEmpyt(){
        if(index < 0 ){
            return true;
        }return false;
    }
}
