package pl.sda.algorytmy.queue;

import java.util.Arrays;
import java.util.Random;
public class Main {
    public static void main(String[] args) {
        Queue q = new Queue();
        q.enqueue(10);
        q.enqueue(20);
        q.enqueue(30);
        q.enqueue(40);
        q.enqueue(50);
        q.dequeue();
        q.dequeue();
        q.dequeue();
        q.enqueue(60);
        System.out.println(q.dequeue().toString());
        System.out.println(q.dequeue().toString());
        System.out.println(q.dequeue().toString());
    }
}
