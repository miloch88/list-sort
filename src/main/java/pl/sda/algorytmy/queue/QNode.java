package pl.sda.algorytmy.queue;

public class QNode {
    private Object value;
    private QNode next;
    public QNode(Object value){
        this.value = value;
    }
    public QNode getNext() {
        return next;
    }
    public void setNext(QNode next) {
        this.next = next;
    }
    @Override
    public String toString() {
        return "QNode{" +
                "value=" + value +
                '}';
    }
}