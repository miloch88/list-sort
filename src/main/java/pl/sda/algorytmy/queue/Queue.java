package pl.sda.algorytmy.queue;


public class Queue {
    QNode front;
    QNode rear;
    public Queue(){
        this.front = null;
        this.rear = null;
    }
    public void enqueue(Object o){
        QNode temp = new QNode(o);
        //jezeli kolejka jest pysta, to nowy element jest jej koncem i poczatkiem
        if (this.rear == null){
            this.front = temp;
            this.rear = temp;
            return;
        }
        //dodajemy element na koncu kolejki i zmieniamy jej rear
        this.rear.setNext(temp);
        this.rear = temp;
    }
    public QNode dequeue(){
        if (this.rear == null){
            return null;
        }
        QNode temp = this.front;
        this.front = this.front.getNext();
        if(this.front == null){
            this.rear = null;
        }
        return temp;
    }
}
