package pl.sda.algorytmy.sortowanie.buble;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        int[] tablica = new int[20];
        Random random = new Random();

        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = random.nextInt(500);
        }

        wypisz(tablica);

        sortBuble(tablica);
        System.out.println();

        wypisz(tablica);


    }

    private static void sortBuble(int[] tablica) {
        int temp;
        for (int i = 0; i < tablica.length; i++) {
            for (int j = 1; j < tablica.length-i; j++) {
                if (tablica[j] < tablica[j - 1]) {
                    temp = tablica[j - 1];
                    tablica[j - 1] = tablica[j];
                    tablica[j] = temp;
                }
            }
        }
// dodaj flagę, która sprawdza czy było sortowanie


    }

    private static void wypisz(int[] tablica) {
        for (int i = 0; i < tablica.length; i++) {
            System.out.print(tablica[i] + " ");
        }
    }
}