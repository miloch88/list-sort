package pl.sda.algorytmy.sortowanie.insert;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        int[] tablica = new int[20];
        Random random = new Random();

        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = random.nextInt(500);
        }

        wypisz(tablica);

        sortInsert(tablica);
        System.out.println();

        wypisz(tablica);
    }

    private static void sortInsert(int[] tablica) {

        int temp;
        for (int i = 1; i < tablica.length; i++) {
            for(int j = i ; j > 0 ; j--){
                if(tablica[j] < tablica[j-1]){
                    temp = tablica[j];
                    tablica[j] = tablica[j-1];
                    tablica[j-1] = temp;
                }
            }
        }

    }




    private static void wypisz(int[] tablica) {
        for (int i = 0; i < tablica.length; i++) {
            System.out.print(tablica[i] + " ");
        }
    }


}

