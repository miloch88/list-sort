package pl.sda.algorytmy.stos.StockLinkedList;

public class Main {
    public static void main(String[] args) {

        StosLinkedList stosLinkedList = new StosLinkedList();

        System.out.println("Stos jest pusty: " + stosLinkedList.isEmpty());
        stosLinkedList.push("Ala");
        stosLinkedList.push(" ma");
        stosLinkedList.push(5);
        stosLinkedList.push("kotów i jednego w głowie");

        stosLinkedList.peek();

        stosLinkedList.pop();
        stosLinkedList.peek();

        stosLinkedList.pop();
        stosLinkedList.peek();

        System.out.println("Stos jest pusty: " + stosLinkedList.isEmpty());

        stosLinkedList.push("pstro w głowie");
        stosLinkedList.peek();
    }
}
