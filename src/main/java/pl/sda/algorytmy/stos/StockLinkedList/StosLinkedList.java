package pl.sda.algorytmy.stos.StockLinkedList;

public class StosLinkedList {

    private ListElement head;
    private ListElement tail;

    private int size;

    public StosLinkedList() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    public void push(Object dane) {
        ListElement element = new ListElement(dane);
        if (size == 0 && head == null && tail == null) {
            this.head = element;
            this.tail = element;
        }

        if (size == 1 && head == tail) {
            this.head.setNext(element);
            this.tail.setPrev(head);
            tail = element;
        }

        if (size > 1 && head != tail) {
            this.tail.setNext(element);
            element.setPrev(this.tail);
            this.tail = element;
        }
        size++;
    }

    public void peek() {
        try {
            System.out.println(this.tail.getValue());
        } catch (Exception e) {
            System.out.println("Stos jest pusty, dodaj element.");
        }
    }

    public void pop() {
        this.tail = tail.getPrev();
        this.tail.setNext(null);
        size--;
    }

    public boolean isEmpty() {
        if (size == 0) {
        return true;
        }return false;
    }
}


