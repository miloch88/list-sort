package pl.sda.algorytmy.stos.stack;

public class ListArray {

    private Object[] tab;
    private int size = 0;

    public int getSize() {
        return size;
    }

    public ListArray() {
        this.tab = new Object[10];
    }

    public ListArray(int iloscElementow) {
        this.tab = new Object[iloscElementow];
    }

    public void add(Object object) {
        // jeśli jest za mało miejsca, rozszerz tablicę
        if (this.size >= this.tab.length) {
            rozszerzTablicę();
        }
        // dopisuje nowy element
        // wstawiamy do tablicy o indeksie [obecny_rozmiar]
        this.tab[size++] = object;
        // po
    }

    @Override
    public String toString() {
        return "ListArray{" +
                "tab=" + toStringTab() +
                ", size=" + size +
                '}';
    }

    /**
     * Wstawienie elementu 'object' na indeks 'index'.
     *
     * @param index
     * @param object
     */
    public void add(int index, Object object) {
        // todo: implementacja
    }

    private String toStringTab() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < size; i++) {
            builder.append(tab[i] + ", ");
        }
        return builder.toString();
    }

    public void remove(int indeks) {
        // iterujemy tablicę od elementu o indeksie wskazanym do usunięcia
        // aż do końca listy (do rozmiaru listy)
        if (indeks > 0) {
            for (int i = indeks; i < size; i++) {
                // wewnątrz pętli
                // przesuwamy element z indeksu o 1 wyższego do elementu aktualnego
                // tab[indeks] = tab[indeks+1];
                this.tab[i] = this.tab[i + 1];
            }

            // redukujemy rozmiar listy i wstawiamy null na końcu (opcjonalne)
            this.size--;
            this.tab[this.size] = null;
        }
    }

    private void rozszerzTablicę() {
        // brakuje nam miejsca w tablicy
        // kopiujemy elmenty z tablicy
        Object[] kopia = new Object[this.tab.length * 2];
        // przepisuje elementy ze starej tablicy do nowej
        for (int i = 0; i < this.tab.length; i++) {
            kopia[i] = this.tab[i];
        }

        // nadpisuję starą tablicę nową tablicą
        this.tab = kopia;
    }

    public int size() {
        return size;
        // nie jest odporne na wstawianie null'i w tablicę
//        for (int i = 0; i < tab.length; i++) {
//            if(tab[i] == null){
//                // jeśli znalazłem null, to znaczy że w tej komórce w tablicy jest pusto
//                return i;
//            }
//        }
//
//        return tab.length;
    }

    public Object get(int indeks) {
        return this.tab[indeks];
    }

    public int indexOf(Object o) {
        for (int i = 0; i < size; i++) {
            if (tab[i] == (o)) {
                return i;
            }
        }
        return -1;
    }

    public boolean contains(Object o) {
        for (int i = 0; i < size; i++) {
            if (tab[i] == (o))
                return true;
        }
        return false;
    }

    public boolean isEmpty(){
        if(size == 0){
            return true;
        }return false;
    }

    public void clear(){
        for (int i = 0; i < size; i++) {
            tab[i]= null;
            size = 0;
        }
    }


}