package pl.sda.algorytmy.stos.stack;

public class Main {
    public static void main(String[] args) {

        Stos stos = new Stos(5);

        stos.push(1);
        stos.push("dwa");
        stos.push(3);
        stos.push("cztery");
        stos.push(5);
        stos.push("sześć");
        stos.push(7);
        stos.push("osiem");
        stos.push(9);

        System.out.println(stos.contains("osiem"));
        System.out.println(stos.contains(8));

    }
}
