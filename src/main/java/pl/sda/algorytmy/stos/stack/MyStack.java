package pl.sda.algorytmy.stos.stack;

public class MyStack<T> {

    private int maxSize;
    private Object[] array;
    private int top;

    public MyStack(int maxsize) {
        this.maxSize = maxsize;
        this.array = new Object[maxsize];
        this.top = -1;
    }

    public void push(Object o) {
        if (this.top < this.array.length - 1) {
            this.array[++this.top] = o;
        } else {
            System.out.println("Stos jest pełen, nie można dodać elelemtnu... ");
        }
    }

    public Object pop() {
        if (top >= 0) {
            return this.array[top--];
        }else System.out.println("Kolejka jest już pusta");
        return -1;
    }

    public Object peek() {
        if (top >= 0) {

            return this.array[top];
        }
        return -1;
    }

    public boolean isEmpty() {
        if (this.top < 0) {
            return true;
        } else {
            return false;
        }
    }

    public int getSize() {
        return top;
    }

        public boolean contains(Object obj) {
        Object o = (Object) obj;
        for (int i = 0; i <= top; i++) {
            if (array[i] == o) {
                return true;
            }
        }
        return false;
    }

    public void clear() {
        for (int i = 0; i < array.length; i++) {
            array[i] = null;
            this.top = -1;
        }
    }
}
