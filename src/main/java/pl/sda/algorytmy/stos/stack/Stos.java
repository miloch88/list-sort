package pl.sda.algorytmy.stos.stack;

public class Stos {

    ListArray stos;
    int maxRozmiar;

    public Stos(int maxRozmiar) {
        this.maxRozmiar = maxRozmiar;
        stos = new ListArray(maxRozmiar);
    }

    public void push(Object obj) {
        stos.add(obj);
    }

    public void pop() {
        try {
            stos.remove(stos.getSize());
        } catch (Exception e) {
            System.out.println("Kolejka jest już pusta");
        }
    }

    public Object peek() {
        try {
            return stos.get(stos.getSize() - 1);
        } catch (Exception e) {
            System.out.println("Kolejka jest pusta");
        }
        return null;
    }

    public boolean isEmeptyOrNot(){
        return stos.isEmpty();
    }

    public int getSize() {
        return stos.getSize();
    }

    public boolean contains(Object obj) {
        return stos.contains(obj);
    }

    public void clear(){
        stos.clear();
    }
}

