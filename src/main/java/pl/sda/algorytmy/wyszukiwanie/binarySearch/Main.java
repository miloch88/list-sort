package pl.sda.algorytmy.wyszukiwanie.binarySearch;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        int[] tablica = new int[20];
        Random random = new Random();
        int b = 0;

        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = random.nextInt(10);
        }

        wypisz(tablica);
        sortBuble(tablica);
        wypisz(tablica);

        System.out.println("Numer indeksu po szukaniu z rekurencją: " + binearSearch(tablica, 4, 0, tablica.length - 1));
        System.out.println("Numer indeksu po szukaniu bez rekurencji: " + binearSearchWithIf(tablica,4));


    }

    private static int binearSearchWithIf(int[] tablica, int i) {

        int L = 0;
        int R = tablica.length - 1;
        while (L <= R) {
            int m = (L + R) / 2;
            if (tablica[m] == i) {
                for (int j = tablica.length-1; j > 0; j--) {
                    if(tablica[j] == i){
                        return j;
                    }
                }
                 return m;
            } else if (tablica[m] < i) {
                L = m + 1;
            } else if (tablica[m] > i){
                R = m - 1;
            }
        }
            return -1;
    }

    private static int binearSearch(int[] tablica, int i, int left, int right) {


        if (left <= right) {
            //kolejność działań !!!!!!!
            int m = (left + right) / 2;
            if (tablica[m] == i) {
                for (int j = 0; j < tablica.length; j++) {
                    if(tablica[j] == i )
                return j;
                }
            } else if (tablica[m] > i) {
                right = m - 1;
                return binearSearch(tablica, i, left, right);
            } else {
                left = m + 1;
                return binearSearch(tablica, i, left, right);
            }
        }
        return -1;
    }


    private static void linearSearch(int[] tablica, int a) {
        for (int i = 0; i < tablica.length; i++) {
            if (a == tablica[i]) {
                System.out.println("Twoja szukana liczba znajduję się na indeksie: " + i);
                return;
            }
        }
        System.out.println("Brak szukanej liczby");
    }

    private static void sortBuble(int[] tablica) {
        int temp;
        for (int i = 0; i < tablica.length; i++) {
            for (int j = 1; j < tablica.length - i; j++) {
                if (tablica[j] < tablica[j - 1]) {
                    temp = tablica[j - 1];
                    tablica[j - 1] = tablica[j];
                    tablica[j] = temp;
                }
            }
        }
    }


    private static void wypisz(int[] tablica) {
        for (int i = 0; i < tablica.length; i++) {
            System.out.print(tablica[i] + " ");
        }
        System.out.println();
    }
}
