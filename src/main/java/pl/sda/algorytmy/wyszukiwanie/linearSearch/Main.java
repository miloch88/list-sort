package pl.sda.algorytmy.wyszukiwanie.linearSearch;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        int[] tablica = new int[20];
        Random random = new Random();
        int b = 0;

        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = random.nextInt(50);
        }

        wypisz(tablica);
        System.out.println();

        linearSearch(tablica, 10);
        System.out.println(linearSearch2(tablica, 10, 0));
        ;


    }

    private static void linearSearch(int[] tablica, int a) {
        for (int i = 0; i < tablica.length; i++) {
            if (a == tablica[i]) {
                System.out.println("Twoja szukana liczba znajduję się na indeksie: " + i);
                return;
            }
        }
        System.out.println("Brak szukanej liczby");
    }

    private static int linearSearch2(int[] tablica, int a, int i) {
        if (i < tablica.length) {

            if (tablica[i] == a) {
                return i;
            } else {
                return linearSearch2(tablica, a, ++i);
            }
        }
        return -1;

//        Nie da się tego zapisać
//        return i < tablica.length && tablica[i] == a ? i b : linearSearch2(tablica, a, ++i);
    }


    private static void sortBuble(int[] tablica) {
        int temp;
        for (int i = 0; i < tablica.length; i++) {
            for (int j = 1; j < tablica.length - i; j++) {
                if (tablica[j] < tablica[j - 1]) {
                    temp = tablica[j - 1];
                    tablica[j - 1] = tablica[j];
                    tablica[j] = temp;
                }
            }
        }
    }


    private static void wypisz(int[] tablica) {
        for (int i = 0; i < tablica.length; i++) {
            System.out.print(tablica[i] + " ");
        }
    }
}
