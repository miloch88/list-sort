package pl.sda.collections.array;

public class Main {
    public static void main(String[] args) {

        ListArray listArray = new ListArray();

        listArray.add(1);
        listArray.add(2);
        listArray.add("trzy");
        listArray.add("Paweł");
        listArray.add(5);
        listArray.add("Sześć");
        listArray.add("Aaaaaaa");

        System.out.println(listArray);

        listArray.remove(3);
        listArray.remove(4);

        System.out.println(listArray);


    }
}
