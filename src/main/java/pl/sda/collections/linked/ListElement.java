package pl.sda.collections.linked;

public class ListElement {
    private Object value;
    private ListElement next; // następinik
    private ListElement prev; // poprzednik

    public ListElement(Object value) {
        this.value = value;
        this.next = null;
        this.prev = null;
            }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public ListElement getNext() {
        return next;
    }

    public void setNext(ListElement next) {
        this.next = next;
    }

    public ListElement getPrev() {
        return prev;
    }

    public void setPrev(ListElement prev) {
        this.prev = prev;
    }
}
