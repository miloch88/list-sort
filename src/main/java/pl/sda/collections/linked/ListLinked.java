package pl.sda.collections.linked;

public class ListLinked {

    private ListElement head;
    private ListElement tail;

    private int size;

    public ListLinked() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    // add(Object o)
    public void add(Object dane) {
        ListElement element = new ListElement(dane);

        if (size == 0 && head == null && tail == null) {
            // lista jest pusta
            this.head = element;
            this.tail = element;
        } else if (size == 1 && head == tail) {
            // jest tylko jeden element
            this.tail = element;
            this.head.setNext(element); // head Twój następnik to element
            element.setPrev(this.head); // element Twój poprzednik to head
        } else {
            // dodaje gdzieś na koniec nowy element
            this.tail.setNext(element);
            element.setPrev(this.tail);
            this.tail = element;
        }
        this.size++;
    }


    private String toStringTab() {
        StringBuilder builder = new StringBuilder();
        ListElement tmp = this.head;
        while (tmp != null) {
            builder.append(tmp.getValue() + " ");
            tmp = tmp.getNext(); // przejście do następnego elementu
        }
        return builder.toString();
    }

    @Override
    public String toString() {
        return "ListLinked{" +
                "data=" + toStringTab() +
                '}';
    }

    public void removeLast() {
        //ostatni to teraz przedostatni
        this.tail = tail.getPrev();
        //następnikiem ostatniego jest null
        this.tail.setNext(null);
        this.size--;
    }

    public void removeFirst(){
        //pierwszym elelementem raje się drugi elelemt
        this.head = this.head.getNext();
        //poprzednikiem pierwszego elelmrnyu jest nul
        this.head.setPrev(null);
        this.size--;
    }

    public void remove(int index) {
//        if(index == 0){
//            removeFirst();
//            return;
//        }

        ListElement tmp = this.head;
        int count = 0;

        //iterujemy po tablicy
        while (tmp != null && count != index) {
            tmp = tmp.getNext(); // przejście do następnego elementu
            count++;
        }

        //koniec listy
        if (tmp == null) {
            throw new ArrayIndexOutOfBoundsException("out of bounds");
        }

        ListElement poprzednik = tmp.getPrev();
        ListElement następnik = tmp.getNext();

        if (poprzednik != null) {
            poprzednik.setNext(następnik);
        } else {
            head = head.getNext();// jeśli usuwam z indexu 0
        }

        if (następnik != null) {
            następnik.setPrev(poprzednik);
        } else {
            tail = tail.getPrev();// jeśli usuwam z indexu ostatniego
        }

        this.size--;
    }

    public int indexOf(Object obj){
        int count = 0;
        ListElement tmp = this.head;
        //musi być taka kolejność jak teraz, a nie inaczej
        while (tmp != null && tmp.getValue() != obj ){
            tmp = tmp.getNext();
            count++;
        }
        //jeśli nie uda nam się znaleźć elelemtntu
        if(tmp == null || tmp.getValue() != obj){
            return -1;
        }
        return count;
    }

    public boolean contains(Object o){
        return indexOf(o) != -1;
    }

    public void clear(){
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    public Object get(int index){
        ListElement tmp = this.head;
        int cout= 0;

        //wykonujemy przejście po elelemntach listy
        while (tmp!= null && index != cout){
            tmp= tmp.getNext();
            cout++;
        }

        //jeśli dotarłem za daleko lub nie odnazlazłem szukanego elelemtu
        if(tmp == null || index != cout){
            return null;
        }
        //jeśli odnalazłem szukany węzeł (indeks) to zwracam jego wartość
        return tmp.getValue();
    }


    // add(Object... o) - dodaje elementy varargs do listy



}