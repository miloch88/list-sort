package pl.sda.collections.linked;

public class Main {
    public static void main(String[] args) {

        ListLinked listLinked = new ListLinked();

        listLinked.add(1);
        listLinked.add(2);
        listLinked.add(3);
        listLinked.add(4);
        listLinked.add(5);
        listLinked.add(6);
        listLinked.add(7);
        listLinked.add(8);

        System.out.println(listLinked);

        System.out.println();
        listLinked.remove(5);
        System.out.println(listLinked);


        System.out.println(listLinked.indexOf(56));
    }
}
